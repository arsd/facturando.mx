<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="DemoApp">
<head>
    <title>Facturando.mx</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <script src="js/jquery-2.2.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="css/dx.common.css" />
    <link rel="dx-theme" data-theme="generic.light" href="css/dx.light.css"  />
    <style>
	body{padding-top:10px;}
	.container {height:auto;width:100%;margin-left:auto;margin-right:auto}
	@media (min-width: 768px) {.container{width:750px;}}
	@media (min-width: 992px) {.container{width:970px;}}
	@media (min-width: 1200px){.container{width:1170px;}}
	#gridRestar{float:right;height:34px;width:195px;}
	</style>
    <script src="js/jszip.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-sanitize.min.js"></script>
    <script src="js/dx.all.js"></script>
	<script src="js/data.js"></script>
    <script src="js/custom.js"></script>
</head>
<body class="dx-viewport" ng-controller="DemoController">
    <div class="container" id="prueba">
    	<img src="img/logotipo.png" />
        <button id="gridRestar" dx-button="buttonOptions">Restablecer Tabla</button>
        <div id="gridContainer" dx-data-grid="dataGridOptions"></div>
        <div dx-context-menu="contextMenuOptions"></div>
    </div>
    <br>
</body>
</html>