<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
		$this->call('PaisesSeeder');
		$this->call('EstadosSeeder');
		$this->call('MunicipiosSeeder');
		$this->call('ClientesSeeder');

        Model::reguard();
    }
}

class PaisesSeeder extends Seeder
{
	public function run()
    {
        DB::table('paises')->delete();
		DB::statement('ALTER TABLE paises AUTO_INCREMENT = 0;');
		$faker = Faker::create();
		
		for($i = 0; $i < 10; $i++) {
			$pais = $faker->country;
			DB::table('paises')->insert([
				'Nombre'=> $pais,
				'Usuario'=> strtoupper(substr($pais,0,3))
			]);
		}
    }

}

class EstadosSeeder extends Seeder
{
	public function run()
    {
        DB::table('estados')->delete();
		DB::statement('ALTER TABLE estados AUTO_INCREMENT = 0;');
		
		$faker = Faker::create();
		
		for($i = 0; $i < 100; $i++) {
			$estado = $faker->state;
			DB::table('estados')->insert([
				'Nombre'=> $estado,
				'Usuario'=> strtoupper(substr($estado,0,3))
			]);
		}
    }

}

class MunicipiosSeeder extends Seeder
{
	public function run()
    {
        DB::table('municipios')->delete();
		DB::statement('ALTER TABLE municipios AUTO_INCREMENT = 0;');
		
		$faker = Faker::create();
		
		for($i = 0; $i < 1000; $i++) {
			$municipio = $faker->state;
			DB::table('municipios')->insert([
				'Nombre'=> $municipio,
				'Usuario'=> strtoupper(substr($municipio,0,3))
			]);
		}
    }

}

class ClientesSeeder extends Seeder
{
	public function run()
    {
        DB::table('clientes')->delete();
		DB::statement('ALTER TABLE clientes AUTO_INCREMENT = 0;');
		
		$faker = Faker::create();
		
		for($i = 0; $i < 100000; $i++) {
			$email = $faker->email;
			DB::table('clientes')->insert([						
				'Rfc'=> rand(pow(10,10),pow(10,11)-1),
				'RazonSocial'=> $faker->company,
				'Telefono'=> $faker->phoneNumber,
				'Status'=> $faker->randomElement([0, 1]),
				'Contribuyente'=> $faker->randomElement([0,1]),
				'SitioWeb'=> 'http://www.'.$faker->domainName,
				'CorreoElectronico'=> $email,
				'CorreoConCopia'=> $email,
				'Calle'=> $faker->streetName,
				'NumeroExterior'=> $faker->buildingNumber,
				'NumeroInterior'=> $faker->buildingNumber,
				'Referencia'=> $faker->streetAddress,
				'Colonia'=> $faker->streetName,
				'Localidad'=> $faker->city,
				'Municipio'=> rand(1, 1000),
				'Estado'=> rand(1, 100),
				'Pais'=> rand(1, 10),
				'FormaPago'=> $faker->randomElement(['chocolate','vainilla','cheesecake']),
				'CondicionesPago'=>$faker->randomElement(['chocolate','vainilla','cheesecake']),
				'MetodoPago'=> $faker->randomElement(['chocolate','vainilla','cheesecake']),
				'NumeroCuentaPago'=> $faker->creditCardNumber,
				'Contacto'=> $faker->name,
				'LimiteCredito'=> $faker->randomFloat(6,100000),
				'FechaAlta'=> $faker->date,
				'HoraAlta'=> $faker->time,
				'FechaBaja'=> $faker->date,
				'HoraBaja'=> $faker->time,
				'EjecutivoAtiende'=> rand(1, 1000),
				'Usuario'=> $faker->userName
			]);
		}
    }

}