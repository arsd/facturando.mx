<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BdInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //TABLA clientes
		Schema::create('clientes', function(Blueprint $table){
			$table->increments('ID');
			$table->string('Rfc',13);
			$table->string('RazonSocial',120);
			$table->string('Telefono',30);
			$table->smallInteger('Status');
			$table->smallInteger('Contribuyente');
			$table->string('SitioWeb',200);
			$table->string('CorreoElectronico',100);
			$table->string('CorreoConCopia',100);
			$table->string('Calle',50);
			$table->string('NumeroExterior',50);
			$table->string('NumeroInterior',50);
			$table->string('Referencia',50);
			$table->string('Colonia',50);
			$table->string('Localidad',100);
			$table->integer('Municipio');
			$table->integer('Estado');
			$table->integer('Pais');
			$table->string('FormaPago',100);
			$table->string('CondicionesPago',100);
			$table->string('MetodoPago',100);
			$table->string('NumeroCuentaPago',20);
			$table->string('Contacto',120);
			$table->decimal('LimiteCredito',18,6);
			$table->date('FechaAlta');
			$table->time('HoraAlta');
			$table->date('FechaBaja');
			$table->time('HoraBaja');
			$table->integer('EjecutivoAtiende');
			$table->string('Usuario',50);
			$table->timestamps();
		});
		
		//TABLA ejecutivos_de_ventas
		Schema::create('ejecutivos_de_ventas', function(Blueprint $table){
			$table->increments('ID');
			$table->string('Nombre',100);
			$table->string('ApellidoPaterno');
			$table->string('ApellidoMaterno');
			$table->date('FechaAlta');
			$table->string('Usuario');
			$table->timestamps();
		});
		
		//TABLA estados
		Schema::create('estados', function(Blueprint $table){
			$table->increments('ID');
			$table->string('Nombre',100);
			$table->string('Usuario',50);
			$table->timestamps();
		});
		
		//TABLA paises
		Schema::create('paises', function(Blueprint $table){
			$table->increments('ID');
			$table->string('Nombre',100);
			$table->string('Usuario',50);
			$table->timestamps();
		});
		
		//TABLA municipios
		Schema::create('municipios', function(Blueprint $table){
			$table->increments('ID');
			$table->string('Nombre',100);
			$table->string('Usuario',50);
			$table->timestamps();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('clientes');
		Schema::drop('ejecutivos_de_ventas');
		Schema::drop('estados');
		Schema::drop('paises');
		Schema::drop('municipios');
    }
}
