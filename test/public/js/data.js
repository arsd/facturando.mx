var customers  = {
	load: function (loadOptions) {
		var filterOptions = loadOptions.filter ? JSON.stringify(loadOptions.filter) : "";   // Getting filter settings
		var sortOptions = loadOptions.sort ? JSON.stringify(loadOptions.sort) : "";  // Getting sort settings
		var requireTotalCount = loadOptions.requireTotalCount; // You can check this parameter on the server side  
											   // to ensure that a number of records (totalCount) is required
		var skip = loadOptions.skip; // A number of records that should be skipped 
		var take = loadOptions.take; // A number of records that should be taken
		var group = loadOptions.group;
		console.log(loadOptions)
		var d = $.Deferred();
		$.getJSON('/clientes', {  
			filter: filterOptions,
			sort: sortOptions,
			requireTotalCount: requireTotalCount,
			skip: skip,
			per_page: take,
			page: skip/take+1,
			group:group
		}).done(function (result) {
			// Data processing here
			d.resolve(result.data, { totalCount: result.total }); 
		});
		return d.promise();
	}
}