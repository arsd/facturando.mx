var DemoApp = angular.module('DemoApp', ['dx']);
DemoApp.controller('DemoController', function DemoController($scope) {	
	$scope.selectedItems = [];
	$scope.showColumnLines = true;
	$scope.showRowLines = true;
	$scope.showBorders = true;
	$scope.rowAlternationEnabled = true;
	
	$scope.dataGridOptions = {
		allowColumnReordering:  true,
		allowColumnResizing: true,
		bindingOptions:{
			showColumnLines: "showColumnLines",
			showRowLines: "showRowLines",
			showBorders: "showBorders",
			rowAlternationEnabled: "rowAlternationEnabled"
		},
		columnAutoWidth: true,
		columns: [
			{dataField:"ID",caption:"ID",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"Rfc",caption:"RFC",allowGrouping:false},
			{dataField:"RazonSocial",caption:"Razón social",allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false},
			{dataField:"Telefono",caption:"Teléfono",allowGrouping:false,allowSorting:false},
			{dataField:"Status",caption:"Status"},
			{dataField:"Contribuyente",caption:"Contribuyente"},
			{dataField:"SitioWeb",caption:"Sitio Web",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"CorreoElectronico",caption:"Correo electrónico",allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"CorreoConCopia",caption:"Correo con copia",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"Calle",caption:"Calle",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"NumeroExterior",caption:"Numero Exterior",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"NumeroInterior",caption:"Numero Interior",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"Referencia",caption:"Referencia",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"Colonia",caption:"Colonia",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false},
			{dataField:"Localidad",caption:"Localidad",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false},
			{dataField:"Municipio",caption:"Municipio",allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false},
			{dataField:"Estado",caption:"Estado"},
			{dataField:"Pais",caption:"País"},
			{dataField:"FormaPago",caption:"Forma de Pago",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"CondicionesPago",caption:"Condiciones de Pago",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"MetodoPago",caption:"Mátodo de Pago",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"NumeroCuentaPago",caption:"Numero de Cuenta de Pago",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"Contacto",caption:"Contacto",allowGrouping:false,allowSorting:false},
			{dataField:"LimiteCredito",caption:"Limite de crédito",alignment:'right',allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"FechaAlta",caption:"Fecha Alta",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"HoraAlta",caption:"Hora Alta",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"FechaBaja",caption:"Fecha Baja",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"HoraBaja",caption:"Hora Baja",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false},
			{dataField:"EjecutivoAtiende",caption:"Ejecutivo que lo atiende"},
			{dataField:"Usuario",caption:"Usuario",visible:false,allowGrouping:false,allowFiltering:false,allowHeaderFiltering:false,allowSorting:false}
		],
		dataSource: customers,
		columnAutoWidth: true,
		columnChooser:{enabled:true,title:"Selector de columna"},
		columnFixing:{enabled: true,emptyPanelText:"Arrastre una columna aquí para ocultarlo"},
		export:{enabled: true,fileName:"Employees",allowExportSelectedData:true,texts: { exportAll: "Exportar todos los datos" , exportSelectedRows: "Exportar filas selecionadas" , exportTo: "Exportar" }},
		filterRow:{visible:true,applyFilter:"auto"},
		groupPanel:{visible:true,emptyPanelText:'Arrastre el encabezado de las columnas Status, Contribuyente, Estado, País o Ejecutivo que lo atiende, aquí para agrupar registros'},
		headerFilter:{visible: true},
		hoverStateEnabled: true,
		loadPanel:{text: "Cargando..."},
		pager:{enabled:true,pageIndex:0,infoText:"Página {0} de {1} ({2} registros)",showPageSizeSelector:true,showNavigationButtons:true,allowedPageSizes: [10, 20, 50, 100],showInfo: true},
		paging:{pageSize:10},
		remoteOperations:{filtering:true,grouping:true,paging:true,sorting:true,summary:true},
		searchPanel:{visible:false},		
		selection:{mode:"multiple"},
		onSelectionChanged:function(data) {	
			 $scope.selectedItems=data.selectedRowsData;	
		}, 
		/*onCellClick: function (data) {
			console.log(data.selectedRowsData);
			$scope.selectedItems = data.selectedRowsData;
		},*/
		/*stateStoring: {enabled: true,type: "localStorage",storageKey: "storage"}*/
	};
	
	$scope.buttonOptions = {
        onClick: function () {
            var grid = $("#gridContainer").dxDataGrid("instance");
                grid.clearFilter();
                grid.clearGrouping()
                grid.clearSelection();
                grid.clearSorting();
                grid.refresh();
        }
    };
	
	$scope.contextMenuOptions = {
		dataSource: [
			{text:"Copiar",opc:"copy"},
			{text:"Imprimir",opc:"print"},
			{text:"Ver",opc:"",items:[{text: "Informacion completa",opc:"info"},{text: "Logotipo",opc:"logo"}]},
			{text:"Seleccionar todos",opc:"select_all"}
		],
		width: 200,
		target: "body",
		selectionMode: "single",
		selectByClick: true,
		onItemClick: function (data) {
			var opc = data.itemData.opc;
			if (opc=="copy"){
				if ($scope.selectedItems.length==0){
					alert("Por favor, seleccione al menos 1 registro para poder copiar");
				}else{
					var temp = $("<textarea>");
					$("body").append(temp);
					var texto ="";
					for (i = 0; i < $scope.selectedItems.length; i++) {
						var texto = texto +
							$scope.selectedItems[i]["Rfc"]+"\t"+
							$scope.selectedItems[i]["RazonSocial"]+"\t"+
							$scope.selectedItems[i]["Telefono"]+"\t"+
							$scope.selectedItems[i]["Status"]+"\t"+
							$scope.selectedItems[i]["Contribuyente"]+"\t"+
							$scope.selectedItems[i]["CorreoElectronico"]+"\t"+
							$scope.selectedItems[i]["Municipio"]+"\t"+
							$scope.selectedItems[i]["Estado"]+"\t"+
							$scope.selectedItems[i]["Pais"]+"\t"+
							$scope.selectedItems[i]["Contacto"]+"\t"+
							$scope.selectedItems[i]["LimiteCredito"]+"\t"+
							$scope.selectedItems[i]["EjecutivoAtiende"]+"\n";
					}
					temp.val(texto).select();
					document.execCommand("copy");
					temp.remove();
				}
			}
			if (opc=="print"){
				var dataGrid = $("#gridContainer").dxDataGrid("instance"); 
                dataGrid.exportToExcel(false);
			}
			if (opc=="select_all"){ 
				var dataGrid = $("#gridContainer").dxDataGrid("instance"); 
                dataGrid.selectAll();
				//dataGrid.deselectAll();
			}
		}
	}
});