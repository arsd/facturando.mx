<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Clientes;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$clientes=  DB::table('clientes');
		$per_page= $request->input('per_page')?$request->input('per_page'):10;
		$skip=     $request->input('skip')?$request->input('skip'):0;
		$filters=  json_decode($request->input('filter')?$request->input('filter'):NULL);
		$orders=   json_decode($request->input('sort')?$request->input('sort'):NULL);
		$groups=   $request->input('group')?$request->input('group'):NULL;
		
		if (!is_null($filters)){
			if (!in_array("and", $filters)){
				if ($filters[1]=="contains") $clientes=$clientes->where($filters[0], 'like', '%'.$filters[2].'%');
				else $clientes=$clientes->where($filters[0], $filters[1], $filters[2]);
			}else{	
				foreach($filters as $filter){
					if ($filter!="and"){
						if ($filter[1]=="contains") $clientes=$clientes->where($filter[0], 'like', '%'.$filter[2].'%');
						else $clientes=$clientes->where($filter[0], $filter[1], $filter[2]);	
					}
				}
			}
		}
		if (!is_null($orders)){
			foreach($orders as $order){
				$desc = $order->desc?'desc':'asc';
				$clientes= $clientes->orderBy($order->selector, $desc);
			}
		}
		
		
		$clientes= $clientes->paginate($per_page);
		if (!is_null($groups)){
			if (isset($groups[0]['isExpanded'])){
				if($groups[0]['isExpanded']=='false'){
					$clientes=$clientes->groupBy($groups[0]['selector']);
				}
			}
		}
		return $clientes;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		Clientes::create($request->all());
        return ['created' => true];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		return Clientes::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$user = Clientes::find($id);
        $user->update($request->all());
        return ['updated' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		Clientes::destroy($id);
        return ['deleted' => true];
    }
}
